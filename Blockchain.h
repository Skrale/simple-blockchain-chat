//
// Created by alex on 28.10.18.
//

#ifndef CPP_TEST_BLOCKCHAIN_H
#define CPP_TEST_BLOCKCHAIN_H

#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>
#include <list>
#include "Block.h"

class Blockchain {
public:
    Blockchain() {
        Block genesis_block;
        chain.insert(chain.end(), genesis_block);
        std::cout << "Blockchain created!" << std::endl;
        std::cout << "Genesis block:" << std::endl;
        std::string genesisStr = chain.back();
        std::cout << genesisStr << std::endl;
    }

    size_t getLastHash() {
        return chain.back().getHash();
    }

    Block& getLastBlock() {
        return chain.back();
    }

    bool insertBlock(Block& new_block) {
        if (isBlockValid(new_block)) {
            chain.insert(chain.end(), new_block);
            return true;
        }
        return false;
    }

    operator std::string() {
        std::stringstream result;
        result << "The contents of blockchain: " << std::endl;
        for (std::list<Block>::iterator it = chain.begin(); it != chain.end(); it++) {
            std::string block = *it;
            result << block << std::endl;
        }
        return result.str();
    }

private:
    bool isBlockValid(Block& target) {
        Block last_block = chain.back();
        /* Simple check that chain last block hash
         * equals to previous block hash in target block */
        if (last_block.getHash() != target.getPrevHash()) {
            return false;
        }
        if (1 != (target.getIndex() - last_block.getIndex())) {
            return false;
        }
        /* Calclulate block hash to prevent attack on blockchain */
        target.setPrevHash(last_block.getHash());
        if (target.CalculateHash() != target.getHash()) {
            return false;
        }
        /* Target block is valid */
        return true;
    }

    std::list<Block> chain;
};

#endif //CPP_TEST_BLOCKCHAIN_H
