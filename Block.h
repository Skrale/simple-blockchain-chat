//
// Created by alex on 28.10.18.
//

#ifndef CPP_TEST_BLOCK_H
#define CPP_TEST_BLOCK_H

#include <sstream>
#include <ctime>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include "Message.h"

class Block {
public:
    Block()
            : PrevHash(0),
              Index(0),
              message("Genesis block"),
              Time(time(NULL))
    {
        Hash = CalculateHash();
    }

    Block(const Message &message) : Block()
    {
        this->message = message;
        Hash = CalculateHash();
    }

    Block(Block& sPrevBlock, const Message &message) : Block(message)
    {
        Index = sPrevBlock.getIndex() + 1;
        PrevHash = sPrevBlock.getHash();
        Hash = CalculateHash();
    }

    operator std::string()
    {
        std::stringstream stream;
        std::tm * tm_time = std::localtime(&Time);
        char buffer[32];
        // Format: Mo, 15.06.2009 20:20:00
        std::strftime(buffer, 32, "%a, %d.%m.%Y %H:%M:%S", tm_time);
        stream << "PrevHash: " << getPrevHash() << std::endl;
        stream << "Hash:     " << getHash()     << std::endl;
        stream << "Id: "       << Index << std::endl;
        stream << "Time: "     << buffer << std::endl;
        stream << "Message:  " << (std::string)message << std::endl;;
        return stream.str();
    }

    size_t CalculateHash() {
        std::hash<std::string> hash_fn;
        std::stringstream stream;
        stream << Index << (std::string)message << PrevHash << Time;
        return hash_fn(stream.str());
    }

    void setPrevHash(size_t value) { PrevHash = value; }
    size_t getPrevHash() { return PrevHash; }
    size_t getHash() { return Hash; }
    uint32_t getIndex() { return Index; }

    // Allow serialization to access non-public data members.
    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive& ar, const unsigned version) {
        ar & Hash & PrevHash & Index & message & Time;
    }

private:
    size_t Hash;
    size_t PrevHash;
    uint32_t Index;
    Message message;
    time_t Time;
};

#endif //CPP_TEST_BLOCK_H
