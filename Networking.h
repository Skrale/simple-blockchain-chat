//
// Created by alex on 28.10.18.
//

#ifndef CPP_TEST_NETWORKING_H
#define CPP_TEST_NETWORKING_H

#include <boost/asio.hpp>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <list>
#include <thread>
#include "Block.h"

using boost::asio::ip::tcp;

class Connection {
protected:
    std::shared_ptr<tcp::socket> sock;
    boost::asio::ip::address address;
    void send(void *data, size_t size);
    size_t recieve(void *data, size_t maxSize);

public:
    Connection() {};
    Connection(std::shared_ptr<tcp::socket> ptr)
        : sock(ptr)
    {
        address = sock->remote_endpoint().address();
        std::cout << "New connection: " << address.to_string() << std::endl;
    };
    void sendBlock(Block& block) {

        std::stringstream ss;
        boost::archive::text_oarchive ar(ss);
        // Serialize block
        ar & block;
        std::string str = ss.str();
        send((void*)str.c_str(), str.size());
    }
    Block recieveBlock() {
        Block restored_block;
        char data[1024];
        size_t len = recieve(data, 1024);
        data[len] = 0;
        std::stringstream ss(data);
        boost::archive::text_iarchive ar(ss);
        // Deserialize block
        ar & restored_block;
        return restored_block;
    }
    boost::asio::ip::address getAddress() { return address; };
};

class ConnectionsPool {
private:
    std::list<Connection> connectionsList;
public:
    ~ConnectionsPool() {
        connectionsList.clear();
    }
    void addConnection(Connection connection)
    {
        connectionsList.insert(connectionsList.end(), connection);
    }
//    void removeConnection(boost::asio::ip::address target) {
//        for (auto it = connectionsList.begin(); it != connectionsList.end(); it++) {
//            Connection connection = *it;
//            if (connection.getAddress() == target) {
//                connectionsList.remove(connection);
//                return;
//            }
//        }
//    }
    void broadcatsBlock(Block block) {
        for (auto it = connectionsList.begin(); it != connectionsList.end(); it++) {
            Connection connection = *it;
            connection.sendBlock(block);
        }
    }
    unsigned int clientsCount() {
        return connectionsList.size();
    }
};


class Client {
private:
    boost::asio::io_service io_service;
    std::string host;
    std::string port;
public:
    Client(std::string host, std::string port);
    Connection connect();
};

class Server {
private:
//    std::thread listner;
    boost::asio::io_service io_service;
    tcp::acceptor acceptor;
public:
    Connection listen();
    Server(std::string port);
};
#endif //CPP_TEST_NETWORKING_H