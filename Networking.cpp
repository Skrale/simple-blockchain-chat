//
// Created by alex on 28.10.18.
//
#include "Networking.h"

using boost::asio::ip::tcp;

void Connection::send(void *data, size_t size) {
    boost::asio::write(*sock, boost::asio::buffer(data, size));
}

size_t Connection::recieve(void *data, size_t maxSize) {
    size_t length = boost::asio::read(*sock,
                                      boost::asio::buffer(data, maxSize),
                                      boost::asio::transfer_at_least(1));
    return length;
}

Client::Client(std::string host, std::string port)
    : host(host),
      port(port) {};

Connection Client::connect() {
    tcp::resolver resolver(io_service);
    resolver.resolve({host, port});
    std::shared_ptr<tcp::socket> sock_ptr(new tcp::socket(io_service));
    return Connection(sock_ptr);
}

Connection Server::listen() {
    boost::asio::io_service io_service;
    tcp::socket sock(io_service);
    acceptor.accept(sock);
    std::shared_ptr<tcp::socket> sock_ptr(new tcp::socket(io_service));
    return Connection(sock_ptr);
};

Server::Server(std::string port)
    : acceptor(io_service, tcp::endpoint(tcp::v4(), std::stoi(port))) {};
