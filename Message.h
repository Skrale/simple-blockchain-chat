//
// Created by alex on 28.10.18.
//

#ifndef CPP_TEST_MESSAGE_H
#define CPP_TEST_MESSAGE_H

#include <stdlib.h>
#include <sstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

class Message {
public:
    Message() : Message("") {};
    Message(const std::string &text)
            : Text(text),
              From("Unknown"),
              To("") {};

    Message(const std::string &from,
            const std::string &text) : Message(text)
    {
        From = from;
    };

    Message(const std::string &from,
            const std::string &to,
            const std::string &text) : Message(from, text)
    {
        To = to;
    };

    operator std::string() {
        std::stringstream result;
        if (To.size()) {
            result << From << " to " << To << ": " << Text;
        }
        else {
            result << From << ": " << Text;
        }
        return result.str();
    }

    // Allow serialization to access non-public data members.
    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive& ar, const unsigned version) {
        ar & Text & From & To;
    }

private:
    std::string Text;
    std::string From;
    std::string To;
};

#endif //CPP_TEST_MESSAGE_H
