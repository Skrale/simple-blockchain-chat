#include <stdlib.h>
#include <fstream>
#include "Blockchain.h"
#include "Networking.h"

#define TEST_NETWORKING
#define TEST_SERIALIZATION

int main(const int argc, const char *argv[])
{
    Blockchain blockchain;
    ConnectionsPool connections;
    Connection connection;
    std::string str("");

#ifdef TEST_SERIALIZATION


#endif
#ifdef TEST_NETWORKING
    try {
        Client client(std::string("127.0.0.1"), std::string("1234"));
        connection = client.connect();
        connections.addConnection(connection);
        Block new_block(blockchain.getLastBlock(), std::string("test message"));
        if (blockchain.insertBlock(new_block)) {
            str = new_block;
            std::cout << str << std::endl;
        }
        connections.broadcatsBlock(new_block);
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception in thread: " << e.what() << "\n";
    }
    try {
        Server server(std::string("1234"));
        connection = server.listen();
        while(true) {
            Block new_block = connection.recieveBlock();
//            if (blockchain.insertBlock(new_block)) {
            str = new_block;
            std::cout << "Recieved block:\n" << str << std::endl;
//            }
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception in thread: " << e.what() << "\n";
    }
#endif

    /* Endless loop */
    while(true) {
        std::string message = "";
        std::cin >> message;

        Message new_message("Alexey", "VOID", message);

        if (!message.compare("exit")) {
            std::cout << "Finishing..." << std::endl;
            break;
        }

        Block new_block(blockchain.getLastBlock(), new_message);
        if (blockchain.insertBlock(new_block)) {
            str = new_block;
            std::cout << str << std::endl;
        }

//        connections.broadcatsBlock(new_block);
    }

    return 0;
}